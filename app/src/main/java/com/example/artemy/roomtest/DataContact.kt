package com.example.artemy.roomtest

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * CONTACTS (DISPLAY_NAME, GENDER, LANGUAGE)
 */

@Entity(tableName = "CONTACTS")
data class DataContact(@PrimaryKey
                       @ColumnInfo(name = "_id")
                       var id: Int? = 1,
                       @ColumnInfo(name = "DISPLAY_NAME")
                       var displayName: String,
                       @ColumnInfo(name = "GENDER")
                       var gender: String?,
                       @ColumnInfo(name = "LANGUAGE")
                       var language: String?,
                       @ColumnInfo(name = "TVAR")
                       val tvar: String
)