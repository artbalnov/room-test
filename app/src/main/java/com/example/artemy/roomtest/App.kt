package com.example.artemy.roomtest

import android.app.Application
import com.facebook.stetho.Stetho

/**
 * Created by artbalnov on 15.01.2018.
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        Stetho.initializeWithDefaults(applicationContext)
    }

}