package com.example.artemy.roomtest

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

/**
 * Created by artbalnov on 15.01.2018.
 */


@Database(entities = [(DataContact::class)], version = 3)
abstract class AppRoomDatabase : RoomDatabase() {
    abstract fun contactDao(): ContactDao



}