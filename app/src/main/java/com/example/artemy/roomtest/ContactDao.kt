package com.example.artemy.roomtest

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query

/**
 * Created by artbalnov on 15.01.2018.
 */

@Dao
interface ContactDao {
    @Query("SELECT * FROM CONTACTS")
   fun getAllContacts() : List<DataContact>
}
