package com.example.artemy.roomtest

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * Created by artbalnov on 15.01.2018.
 */
class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, "TestDb", null, 3) {

    private val CREATE_TABLE_CONTACTS_SQL = "create table CONTACTS (_id INTEGER primary key autoincrement, DISPLAY_NAME TEXT not null, GENDER TEXT, LANGUAGE TEXT)"


    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CREATE_TABLE_CONTACTS_SQL)

        db.execSQL("insert into CONTACTS (DISPLAY_NAME, GENDER, LANGUAGE) VALUES('user1','male','en')")
        db.execSQL("insert into CONTACTS (DISPLAY_NAME, GENDER, LANGUAGE) VALUES('user2','male','en')")
        db.execSQL("insert into CONTACTS (DISPLAY_NAME, GENDER, LANGUAGE) VALUES('user3','male','en')")
        db.execSQL("insert into CONTACTS (DISPLAY_NAME, GENDER, LANGUAGE) VALUES('user4','male','en')")
        db.execSQL("insert into CONTACTS (DISPLAY_NAME, GENDER, LANGUAGE) VALUES('user5','male','en')")

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
      db.execSQL("ALTER TABLE CONTACTS ADD COLUMN TVAR TEXT default 'suka'")
    }




}